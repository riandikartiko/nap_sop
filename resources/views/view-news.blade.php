@extends('Layouts.master')
@section('title') {{'News - '.$news->title}} @endsection
@section('content')
<div class="container">
  <section class="mt-5 pb-3 wow fadeIn">
    @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    <script>
        toastr.success({{ session('success') }})
    </script>
    @endif
    <div class="row">
      <div class="col-md-12">
        <div class="card card-cascade wider reverse">
          <div class="view view-cascade overlay">
            <img src="{{url('storage/'.$news->thumbnail_path)}}" alt="Wide sample post image" class="img-fluid" style="width:100%;">
          </div>
          <div class="card-body card-body-cascade text-center">
            <h2><a><strong>{{$news->title}}</strong></a></h2>
            <p>Written by <a>{{$news->name_user}}</a>, {{$news->created_at->isoFormat('dddd, D MMMM Y')}}</p>
          </div>
        </div>
        <div class="excerpt mt-5 wow fadeIn" data-wow-delay="0.3s">
          {!!$news->contents !!}
        </div>

      </div>
    </div>
  </section>
</div>
@endsection