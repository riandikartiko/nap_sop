@section('announcement-html')
  <section class="mt-md-4 pt-md-2 mb-3 my-3">
    <div class="container-fluid">
      <div class="card card-cascade narrower">
        <section>
          <div class="row">
            <div class="col-12 mr-0">
              <div class="row">
                <div class="col-12">
                  <section class="row">
                    <div class="col-12 col-md-3">
                      <div class="view view-cascade gradient-card-header blue-gradient">
                        <h4 class="h4-responsive mb-0 font-weight-bold">Announcement</h4>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
              <div class="card-body card-body-cascade pb-0">
                <div class="container-fluid">
                  <div class="row card-body pt-1">
                    <div class="col-12">
                      <livewire:components.landing.announcement-home />
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </section>
        <a class="btn bg-transparent btn-outline-primary">See All</a>
      </div>
  </div>
  </section>
@endsection