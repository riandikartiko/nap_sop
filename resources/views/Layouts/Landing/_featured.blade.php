@section('featured-html')
<section class="mt-md-4 pt-md-2 mb-5">
    <!-- Card -->
    <div class="card card-cascade narrower">
      <!-- Section: Chart -->
      <section>
        <!-- Grid row -->
        <div class="row">
          <!-- Grid column -->
          <div class="col-sm-12 mr-0">
            <!-- Card image -->
            <div class="row">
              <div class="col-sm-12">
                <section class="row">
                  <div class="col-sm-12 col-sm-3">
                    <div class="view view-cascade gradient-card-header blue-gradient">
                      <h4 class="h4-responsive mb-0 font-weight-bold">Featured</h4>
                    </div>
                  </div>
                </section>
              </div>
            </div>
            <!-- Card content -->
            <div class="card-body card-body-cascade pb-0">
              <!-- Panel data -->
              <div class="row card-body pt-3">
                <!-- First column -->
                <div class="col-12">
                    @include('Layouts.Tools.landing._carousel')
                    @yield('carousel-html')
                </div>
                <!-- First column -->
              </div>
              <!-- Panel data -->
            </div>
            <!-- Card content -->
          </div>
        </div>
        <!-- Grid row -->
      </section>
      <!-- Section: Chart -->
      <a class="btn bg-transparent btn-outline-primary" href="{{route('view.allsop')}}">See All</a>
    </div>
    <!-- Card -->

  </section>
@endsection