@section('hc-section-html')
<div class="col-md-12">
    <section class="row">
        <div class="col-md-6 col-12">
            <section class="mb-5">
                <div class="card card-cascade narrower">
                    <div class="row">
                      <div class="col-12 mr-0">
                        <div class="row">
                          <div class="col-12">
                            <section class="row">
                              <div class="col-12 col-md-6">
                                <div class="view view-cascade gradient-card-header blue-gradient">
                                  <h4 class="h4-responsive mb-0 font-weight-bold">HC Corner</h4>
                                </div>
                              </div>
                            </section>
                          </div>
                        </div>
                        <div class="card-body card-body-cascade pb-0">
                          <div class="row card-body pt-3">
                            <div class="col-sm-12">
                              <div class="row">
                                <div class="col-sm-12">
                                  <div class="container fluid">
                                    <section class="row">
                                      <livewire:components.landing.hc-section />
                                    </section>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </section>
        </div>
        <div class="col-md-6 col-12">
            <section class="mb-5">
                <div class="card card-cascade narrower">
                    <div class="row">
                      <div class="col-12 mr-0">
                        <div class="row">
                          <div class="col-12">
                            <section class="row">
                              <div class="col-12 col-md-6">
                                <div class="view view-cascade gradient-card-header blue-gradient">
                                  <h4 class="h4-responsive mb-0 font-weight-bold">Matrix Tips</h4>
                                </div>
                              </div>
                            </section>
                          </div>
                        </div>
                        <div class="card-body card-body-cascade pb-0">
                          <div class="row card-body pt-3">
                            <div class="col-sm-12">
                              <div class="row">
                                <div class="col-sm-12">
                                  <section class="row">
                                    <livewire:components.landing.matrix-tips />
                                  </section>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </section>
        </div>
    </section>
</div>


@endsection