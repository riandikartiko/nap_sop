@section('featured-html')
<section class="mt-md-4 pt-md-2 mb-5">
    <!-- Card -->
    <div class="card card-cascade narrower">
      <!-- Section: Chart -->
      <section>
        <!-- Grid row -->
        <div class="row">
          <!-- Grid column -->
          <div class="col-12 mr-0">
            <!-- Card image -->
            <div class="col-3">
                <div class="view view-cascade gradient-card-header primary-color">
                    <h4 class="h4-responsive mb-0 font-weight-bold">Featured</h4>
                  </div>
            </div>
            <!-- Card content -->
            <div class="card-body card-body-cascade pb-0">
              <!-- Panel data -->
              <div class="row card-body pt-3">
                <!-- First column -->
                <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

                    <!--Controls-->
                    <div class="controls-top">
                      <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                      <a class="btn-floating" href="#multi-item-example" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                    </div>
                    <!--/.Controls-->
                
                    <!--Indicators-->
                    <ol class="carousel-indicators">
                      <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
                      <li data-target="#multi-item-example" data-slide-to="1"></li>
                    </ol>
                    <!--/.Indicators-->
                
                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">
                
                      <!--First slide-->
                      <div class="carousel-item active">
                        <div class="row mb-4">
                          <div class="col-md-4">
                            <div class="card mb-2">
                              <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg"
                                   alt="Card image cap">
                            </div>
                          </div>
                
                          <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                              <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg"
                                   alt="Card image cap">
                            </div>
                          </div>
                
                          <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                              <img class="card-img-top" src="https://free4kwallpapers.com/uploads/originals/2020/08/04/beautiful-landscape-wallpaper.jpg"
                                   alt="Card image cap">
                            </div>
                          </div>
                        </div>
                
                      </div>
                      <!--/.First slide-->
                
                      <!--Second slide-->
                      <div class="carousel-item">
                
                        <div class="row mb-4">
                          <div class="col-md-4">
                            <div class="card mb-2">
                              <img class="card-img-top" src="https://free4kwallpapers.com/uploads/originals/2020/08/04/beautiful-landscape-wallpaper.jpg"
                                   alt="Card image cap">
                            </div>
                          </div>
                
                          <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                              <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(47).jpg"
                                   alt="Card image cap">
                            </div>
                          </div>
                
                          <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                              <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(48).jpg"
                                   alt="Card image cap">
                            </div>
                          </div>
                        </div>
                
                      </div>
                      <!--/.Second slide-->
                    </div>
                    <!--/.Slides-->
                
                  </div>
                <!-- First column -->
              </div>
              <!-- Panel data -->
            </div>
            <!-- Card content -->
          </div>
        </div>
        <!-- Grid row -->
      </section>
      <!-- Section: Chart -->
    </div>
    <!-- Card -->
  </section>
@endsection