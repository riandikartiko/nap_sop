@section('announcement-html')
<div class="row">
    <div class="col-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="far fa-newspaper"></i>
                    Announcement
                </h3>
            </div>
            <div class="card-body">
                <section class="main">
                    <div class="container">
                          <div class="row">
                              <div class="col-12">
                                  <section class="row">
                                    <livewire:components.landing.announcement-home />
                                  </section>
                              </div>
                          </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection