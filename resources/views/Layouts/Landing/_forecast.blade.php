@section('forecast-section-html')
<section class="">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-6">
        <div
             id="wrapper-bg"
             class="card text-white bg-image shadow-4-strong"
             style="background-image: url('img/clouds.gif')"
             >
          <!-- Main current data -->
          <div class="card-header p-4 border-0">
            <div class="row">
              <div class="col-4">
                <div class="text-center mb-3">
                  <p class="h2 mb-1" id="wrapper-name"></p>
                  <p class="mb-1" id="wrapper-description"></p>
                  <p class="display-4 mb-1" id="wrapper-temp"></p>
                </div>
              </div>
              <div class="col-8">
                <div class="row text-center">
                  <div class="row">
                    <div class="col-2">
                      <strong class="d-block mb-2">Now</strong>
                      <img id="wrapper-icon-hour-now" src="" class="" alt="" />
                      <strong class="d-block" id="wrapper-hour-now"></strong>
                    </div>
      
                    <div class="col-2">
                      <strong class="d-block mb-2" id="wrapper-time1"></strong>
                      <img id="wrapper-icon-hour1" src="" class="" alt="" />
                      <strong class="d-block" id="wrapper-hour1"></strong>
                    </div>
      
                    <div class="col-2">
                      <strong class="d-block mb-2" id="wrapper-time2"></strong>
                      <img id="wrapper-icon-hour2" src="" class="" alt="" />
                      <strong class="d-block" id="wrapper-hour2"></strong>
                    </div>
      
                    <div class="col-2">
                      <strong class="d-block mb-2" id="wrapper-time3"></strong>
                      <img id="wrapper-icon-hour3" src="" class="" alt="" />
                      <strong class="d-block" id="wrapper-hour3"></strong>
                    </div>
      
                    <div class="col-2">
                      <strong class="d-block mb-2" id="wrapper-time4"></strong>
                      <img id="wrapper-icon-hour4" src="" class="" alt="" />
                      <strong class="d-block" id="wrapper-hour4"></strong>
                    </div>
      
                    <div class="col-2">
                      <strong class="d-block mb-2" id="wrapper-time5"></strong>
                      <img id="wrapper-icon-hour5" src="" class="" alt="" />
                      <strong class="d-block" id="wrapper-hour5"></strong>
                    </div>
                  </div>
                  <div class="row">
                  <span class=""
                        >Pressure: <span id="wrapper-pressure"></span
                    ></span>
                  <span class="mx-2">|</span>
                  <span class=""
                        >Humidity: <span id="wrapper-humidity"></span
                    ></span>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection
            