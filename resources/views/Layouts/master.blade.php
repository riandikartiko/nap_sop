<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>NAP Info - @yield('title')</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{ asset('dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/mdb.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <script src="{{ asset('plugins/jquery/jquery.js') }}"></script>
  <script defer src="{{ asset('plugins/alpinejs/cdn.min.js') }}"></script>
  <script src="{{ asset('plugins/pdf.js/pdf.js') }}"></script>
  <script src="{{ asset('plugins/mark.js/mark.min.js') }}"></script>
  @livewireScripts
</head>
<body class="hidden-sn white-skin">
  @include('Layouts.Includes._navside')
  <main>
    @yield('content')
    
  </main>
  @include('Layouts.Includes._footer')
  @include('Layouts.Tools.landing._modal-results')
  @include('Layouts.Tools.landing._search-input')
  @yield('modal-results-html')
  @yield('infinity-scroll-search')
  @yield('search-method')

  
  @if (session()->has('success'))
  <script>
      toastr.success({{ session('success') }})
  </script>
  @endif
  <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  {{-- <!-- Bootstrap 4 -->
  <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script> --}}
  <script type="text/javascript" src="{{ asset('dist/js/popper.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('dist/js/mdb.min.js') }}"></script>
  <script>
    $(".button-collapse").sideNav();
    var container = document.querySelector('.custom-scrollbar');
    var ps = new PerfectScrollbar(container, {
      wheelSpeed: 2,
      wheelPropagation: true,
      minScrollbarLength: 20
    });
  </script>
  {{-- <script type="text/javascript">
    window.onload = function() {
      navigator.geolocation.watchPosition(function (position) {
        console.log(position);
        var latitude 	= position.coords.latitude;
        var longitude 	= position.coords.longitude;
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
          type 	: 'POST',
          url		: '{{route("logger")}}',
          data  : {latitude:latitude, longitude:longitude},
          success	: function (e) {
            if (e) {
              $('#lokasi').html(e);
            }else{
              $('#lokasi').html('Tidak Tersedia');
            }
          }
        })
      }, function (e) {
          alert('Geolocation Tidak Mendukung Pada Browser Anda');
      }, {
          enableHighAccuracy: true
      });
    };
  </script> --}}
</body>
</html>
