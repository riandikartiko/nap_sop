@section('carousel-html')
	<div class="row">
        <div class="col-sm-12">
		<div class="MultiCarousel carousel" data-items="2,3,5,6" data-slide="2" id="MultiCarousel"  data-interval="500">
            <div class="MultiCarousel-inner">
                <livewire:components.landing.carousel-home />
            </div>
            <button class="btn btn-primary leftLst"><</button>
            <button class="btn btn-primary rightLst">></button>
        </div>
        </div>
	</div>
@endsection