
@section('search-input-html')
<div class="row">
    <div class="col-6 offset-6 align-self-end">
        <section>
            <div class="input-group"> 
                <input class="form-control form-control-lg"
                type="search"
                name="searchModal"
                id="searchModal"
                data-toggle="modal" data-target="#searchResult"
                placeholder='Search... '
                autocomplete="off" readonly="readonly">
                <div class="input-group-append">
                    <button type="button" class="btn btn-lg btn-default" data-toggle="modal" data-target="#searchResult">
                        <i class="fa fa-search"></i>
                    </button>
                </div> 
            </div>
        </section>
    </div> 
</div>
<br>
@endsection