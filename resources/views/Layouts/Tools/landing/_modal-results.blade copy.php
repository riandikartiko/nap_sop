@section('modal-results-html')
<div class="modal fade" role="document" id="searchResult">
    <div class="modal-dialog modal-dialog-scrollable modal-xl mw-100 w-90">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#17a2b8;">
                <div class="input-group" x-data="{isTyped: false}"> 
                    <input class="form-control form-control-lg"
                    type="search"
                    name="search"
                    id="search"
                    x-ref="searchField"
                    x-on:input="isTyped = ($event.target.value != '')"
                    placeholder='Search... '
                    autocomplete="off"
                    oninput="fetchResult()"
                    x-on:keydown.window.prevent.slash="$refs.searchField.focus()"
                    x-on:keyup.escape="isTyped = false; $refs.searchField.blur()">
                    <div class="input-group-append">
                        <button onclick="resetSearch()" type="submit" class="btn btn-lg btn-default">
                            <i class="fa fa-redo"></i>
                        </button>
                    </div>
                    <div class="input-group-append">
                        <button type="button" class="btn btn-lg btn-default close" onclick="resetSearch()" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                    <livewire:components.landing.modal-search />
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-warning" onclick="resetSearch()">Clear Result</button>
                <button type="button" class="btn btn-default" onclick="resetSearch()" data-dismiss="modal">Close</button>                
            </div>
        </div>
    </div>
</div>  
@endsection
@section('infinity-scroll-search')
<script>
    jQuery(function($) {
        $('.modal-body').on('scroll', function() {
            if ($(this).scrollTop() +
                $(this).innerHeight() >= 
                $(this)[0].scrollHeight) {
                    setTimeout(() => { window.livewire.emit('post-data'); }, 250);
            }
        });
    });
</script>
@endsection
@section('search-method')
<script type="text/javascript">
    function resetSearch() {
      document.getElementById("search").value = '';   
      window.livewire.emit('reset-search');
      window.livewire.emit('$refresh');
    };
    function fetchResult() {
      var x = document.getElementById("search");
      if(x.value.length < 2) {
          window.livewire.emit('reset-search');
      }
      else {
          window.livewire.emit('search-result', { search : x.value});
      }
    };
</script>
@endsection
