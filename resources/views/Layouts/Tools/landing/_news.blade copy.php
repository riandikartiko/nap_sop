@section('news-html')
    <div class="row">
        <div class="col-12">
            <section class="row">
                <div class="col-12 col-md-8">
                    <div id="featured" class="carousel slide carousel" data-ride="carousel">
                        <ol class="carousel-indicators top-indicator">
                            <li data-target="#featured" data-slide-to="0" class="active"></li>
                            <li data-target="#featured" data-slide-to="1"></li>
                            <li data-target="#featured" data-slide-to="2"></li>
                            <li data-target="#featured" data-slide-to="3"></li>
                            <li data-target="#featured" data-slide-to="4"></li>
                        </ol>
                        <div class="carousel-inner">
                            <livewire:components.landing.news-home />
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#featured" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#featured" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="col-12 col-md-4">
                    <div class="row">
                        <div class="col-12">
                            <section class="row">
                                <livewire:components.landing.news-home-sec />
                            </section>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection