<header>
    <!-- Sidebar navigation -->
    <div id="slide-out" class="side-nav wide sn-bg-4">
      <ul class="custom-scrollbar">
        <!-- Logo -->
        <li class="logo-sn waves-effect py-4">
          <div class="text-center">
            <a href="#" class="pl-0"><img src="{{asset('logo/matrix-logo.png')}}"></a>
          </div>
        </li>
        <!-- Search Form -->
        <li>
          <form class="search-form" role="search">
            <div class="md-form mt-0 waves-light">
              <input type="text" class="form-control py-2" name="searchModal" id="searchModal"
              placeholder="Search SOP" data-toggle="modal" data-target="#searchResult" 
              autocomplete="off" readonly="readonly">
            </div>
          </form>
        </li>
        
        <!-- Side navigation links -->
        <li>    
          <ul class="collapsible collapsible-accordion">
            <li>
                <a href="{{route('home')}}" class="collapsible-header waves-effect"><i class="w-fa fas fa-home"></i>Home</a>
            </li>
            {{-- <li>
              <a href="{{route('home')}}" class="collapsible-header waves-effect"><i class="w-fa fas fa-home"></i>News</a>
            </li>
            <li>
              <a href="{{route('home')}}" class="collapsible-header waves-effect"><i class="w-fa fas fa-home"></i>Announcement</a>
            </li> --}}
            @if(Auth::user()->hasAnyRole(array('admin', 'superadmin')))
            <li>
                <a class="collapsible-header waves-effect arrow-r">
                  <i class="w-fa far fa-check-square"></i>Admin Section<i class="fas fa-angle-down rotate-icon"></i>
                </a>
                <div class="collapsible-body">
                  <ul>
                    @if(Auth::user()->hasRole(array('superadmin')))
                    <li>
                      <a href="{{route('add.user')}}" class="waves-effect">Add User</a>
                    </li>
                    <li>
                      <a href="{{route('view.activity')}}" class="waves-effect">Activity Log</a>
                    </li>
                    @endif
                    @if(Auth::user()->hasAnyDept(array('rnc')) || Auth::user()->hasRole(array('superadmin')))
                    <li>
                        <a href="{{route('viewUpload')}}" class="waves-effect">SOP</a>
                    </li>
                    {{-- <li>
                        <a href="{{route('viewUpdate')}}" class="waves-effect">Update File SOP</a>
                    </li> --}}
                    {{-- <li>
                        <a href="#" class="waves-effect">Delete File SOP</a>
                    </li> --}}
                    @endif
                    @if(Auth::user()->hasAnyDept(array('mrc')) || Auth::user()->hasRole(array('superadmin')))
                    <li>
                      <a href="{{route('createNews')}}" class="waves-effect">News</a>
                    </li>
                    <li>
                      <a href="{{route('crAnnouncement')}}" class="waves-effect">Announcement</a>
                    </li>
                    <li>
                      <a href="{{route('createTips')}}" class="waves-effect">Matrix Tips</a>
                    </li>
                    @endif

                    @if(Auth::user()->hasAnyDept(array('hcl')) || Auth::user()->hasRole(array('superadmin')))
                    <li>
                      <a href="{{route('createCorner')}}" class="waves-effect">HC Corner</a>
                    </li>
                    @endif
                  </ul>
                </div>
            </li>
            @endif
          </ul>
        </li>
        <!-- Side navigation links -->
      </ul>
      <div class="sidenav-bg mask-strong"></div>
    </div>
    <!-- Sidebar navigation -->
    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav navbar-dark" style="background-color: #151A48 !important;">
      <div class="float-left">
        <a href="#" data-activates="slide-out" class="button-collapse" style="color: white;"><i class="fas fa-bars"></i> Menu</a>
      </div>
      <!-- Navbar links -->
      <ul class="nav navbar-nav nav-flex-icons ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown"aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user"></i> <span class="clearfix d-none d-sm-inline-block">{{auth()->user()->name}}</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">Log Out</a>
          </div>
        </li>
      </ul>
      <!-- Navbar links -->
    </nav>
    <!-- Navbar -->

  </header>