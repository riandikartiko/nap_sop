@extends('Layouts.master')
@section('content')
<main class="mt-0 pt-0">
    <div class="container">
      <section class="text-center text-lg-left wow fadeIn" data-wow-delay="0.3s">
        <h2 class="text-center h1">Recent SOP</h2>
      </section>
      {{-- <livewire:components.sop.paginate-news-dept /> --}}
      @foreach($sops as $sop)
        <hr class="mb-5">
        <!-- Grid row -->
        <div class="row">
          <!-- Grid column -->
          <div class="col-lg-4 mb-4">
            <!-- Featured image -->
            <div class="view overlay z-depth-1">
              <img src="https://mdbootstrap.com/img/Photos/Others/img (38).jpg" class="img-fluid" alt="">
              <a>
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
          </div>
          <div class="col-lg-7 mb-4">
            <a href="{{route('view.sop.dept', ['id'=>$sop->id_dept])}}" class="red-text">
              <h6 class="pb-1"><strong> {{$sop->dept_name}}</strong></h6>
            </a>
            <h4 class="mb-3"><strong>{{$sop->title}}</strong></h4>
            <p> {{$sop->created_at->isoFormat('dddd, D MMMM Y')}}</p>
            <a class="btn btn-primary mt-2" href="{{route('view.id', ['id'=>$sop->id_post])}}">Read more</a>
          </div>
        </div>
      @endforeach
      <hr>
      <div class="d-xl-flex justify-content-center">
        {!! $sops->links() !!}
      </div>
      <hr>
    </div>

  </main>

@endsection