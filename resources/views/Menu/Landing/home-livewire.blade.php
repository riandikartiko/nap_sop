@extends('Layouts.home')
@section('content')
<div class="view" style="background-image: url({{asset('landing/landing.jpg')}}); background-repeat: no-repeat; background-size: cover; background-position: center center;height: 45vh;">
  <div class="mask rgba-blue-light">
    <div class="container h-100 d-flex justify-content-center align-items-center">
      <div class="row">
        <div class="col-12">
          <div class="text-center">
            <h1 class="h1-reponsive white-text text-uppercase font-weight-bold mb-3"><strong>Halo, {{auth()->user()->name}}</strong></h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('Layouts.Landing._featured')
@include('Layouts.Landing._forecast')
@include('Layouts.Landing._announcement')
@include('Layouts.Landing._news')
@include('Layouts.Landing._hc-section')
<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            {{-- @yield('search-input-html') --}}
            {{-- @yield('forecast-section-html') --}}
            @yield('featured-html')
            @yield('announcement-html')
            @yield('news-main-html')
            @yield('hc-section-html')
        </div>
    </section>
</div>

@endsection




