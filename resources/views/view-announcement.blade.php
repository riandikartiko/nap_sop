@extends('Layouts.master')
@section('title') {{'News - '.$announcement->title}} @endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-12">
                {{-- <h1 class="m-0">{{$news->title}}</h1> --}}
            </div>
          </div>
      </div>
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="card-body" style="display: block;">
            <div>
              <div class="row justify-content-center">
                <div class="d-flex justify-content-center">
                  <span></span>
                  <h4 class="display-4">{{$announcement->title}}</h4>
                </div>
              </div>
              <div class="row justify-content-center">
                <div class="d-flex justify-content-center">
                  {{-- <span><h6>{{$news->created_at->format('l, d M Y')}} - {{$news->name_user}}</h6></span> --}}
                  <span><h6>{{$announcement->created_at->isoFormat('dddd, D MMMM Y')}}</h6></span>
                </div>                    
              </div>
              <div class="row justify-content-center">
                <div class="d-flex justify-content-center">
                  {{-- <span><h6>{{$news->created_at->format('l, d M Y')}} - {{$news->name_user}}</h6></span> --}}
                  <span><h6>{{$announcement->dept_name}}</h6></span>
                </div>                    
              </div>
              <hr>
            </div>
            <hr>
            <section class="content">
              <div class="container-fluid">
                <div class="col-12">
                  {!! $announcement->contents !!}
                </div>
              </div>
            </section>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
</div>
@endsection