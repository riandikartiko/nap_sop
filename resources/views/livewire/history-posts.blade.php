    
    
    @foreach ($history_content as $history)
        
    <div class="time-label">
        <span class="bg-red">{{$history->created_at}}</span>
    </div>
    <div>
        <i class="fas fa-envelope bg-blue"></i>
        <div class="timeline-item">
            <span class="time"><i class="fas fa-clock"></i> 12:05</span>
            <h3 class="timeline-header"><a href="#">Admin</a> {{ $newPosts }}</h3>
            <div class="timeline-body">
                {{ $history->title}} - {{ $history->desc}}
            </div>
            <div class="timeline-footer">
                <a class="btn btn-primary btn-sm">Read more</a>
                {{-- <a class="btn btn-danger btn-sm">Delete</a> --}}
            </div>
        </div>
    </div>
      <!-- END timeline item -->
      <!-- timeline item -->
    @endforeach
    <div>
        <i class="fas fa-clock bg-gray"></i>
    </div>
