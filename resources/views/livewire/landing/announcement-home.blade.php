<div>
    <section class="row">
        @foreach ($announcement as $ann)
            <div class="col-xl-4 my-3">
                    <div class="card shadow border-0 rounded-0 text-light overflow zoom">
                        <div class="position-relative">
                            <div class="ratio_right-cover-2 image-wrapper">
                                <a href="{{route('announcement.title', ['title' => $ann->seo_url])}}">
                                    <div class="pdf-thumbnail">
                                        <img class="img-fluid"
                                            src="{{url('storage/'.$ann->thumbnail_path)}}"
                                            alt="simple blog template bootstrap"
                                            style="width:100%;height:100%;object-fit: cover;">
                                    </div>
                                </a>
                            </div>
                            <div class="position-absolute p-2 p-md-3 b-0 w-100 bg-shadow">
                                <a href="">
                                    <h2 class="h5 text-white my-1">{{$ann->title}}</h2>
                                    <h2 class="h6 text-white">{{$ann->created_at->isoFormat('dddd, D MMMM Y')}}</h2>
                                </a>
                            </div>
                        </div>
                    </div> 
            </div>
        @endforeach
    </section>
</div>

