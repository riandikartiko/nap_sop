    
<div>
    @php
        $count_carousel_hc = 0;
    @endphp
    <div id="tips-carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
    @foreach ($tips as $tp)
    @if ($count_carousel_hc == 0)
        <div class="carousel-item active">
    @else
        <div class="carousel-item">
    @endif
        <div class="position-relative">
            <div class="single-blog video-style">
                <div class="thumb">
                    <img class="img-fluid" src="{{url('storage/'.$tp->thumbnail_path)}}" alt="" >
                </div>
                <div class="short_details">
                    <div class="meta-top d-flex">
                        <a href="#">{{$tp->created_at->format('l, d M Y')}}</a>
                    </div>
                    <a class="d-block" href="{{route('tips.title', ['title'=>$tp->seo_url])}}">
                        <h4>{{$tp->title}}</h4>
                    </a>
                </div>
            </div> 
        </div>
    </div>
    @php
        $count_carousel_hc++;
    @endphp
    @endforeach
        </div>
        <a class="carousel-control-prev" href="#tips-carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#tips-carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
