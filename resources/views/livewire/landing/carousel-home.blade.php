
<div>
    @foreach ($sops as $sp)
        <div class="item">
            <div>
                <a href="{{route('sop.title', ['title'=>$sp->seo_url])}}" target="_blank">
                    <img class="img-fluid" data-pdf-thumbnail-file="{{url('/view/isolate/'.trim($sp->file_path,'/storage/uploads/'))}}" alt="{{$sp->title}}">
                </a>
                <div class="overlay">
                    <div class="text">
                        <a href="{{route('sop.title', ['title'=>$sp->seo_url])}}" style="color:white;">{{$sp->title}}</a>
                    </div>
                </div>
                
            </div>
        </div>
    @endforeach
</div>
