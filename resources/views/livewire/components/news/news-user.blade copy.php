<div>
    @foreach ($posts as $post)

    <article class="blog-post">
        <div class="featured-post">
            <a href="#">
                <img src="https://i.picsum.photos/id/1058/1200/600.jpg" alt="">
            </a>
        </div>
        <div class="divider25"></div>
        <div class="content">
            <h3 class="title">
                <a href="#">{{$post->title}}</a>
            </h3>
            <ul class="meta-post">
                <li class="date">
                    <a href="#">
                        {{$post->created_at->isoFormat('dddd, D MMMM Y')}}
                    </a>
                </li>
            </ul>
            <div class="entry-post">
                {{-- <p>This is a Rebel that surrendered to us. Although he denies it, I believe there may be more of them, and I request permission to conduct a further search of the area. He was armed only with this. Good work, Commander. Leave us. Conduct your search and bring his companions to me. Yes, my Lord.</p> --}}
                <div class="more-link">
                    <a href="#" class="read-more-btn">Read More</a>
                </div>
            </div>
        </div>
    </article>  
    @endforeach
    <div class="d-flex justify-content-center">
        {!! $posts->links() !!}
    </div>
</div>
