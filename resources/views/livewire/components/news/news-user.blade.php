<div>
    <div class="row">
        
        <div class="col col-xs-12">
            <div class="blog-grids">
                @foreach ($posts as $post)
                <div class="grid">
                    <div class="entry-media">
                        <img src="{{url('storage/uploads/news/5-lorem-ipsum-dolor-sit/thumbnail/WIN_20210803_02_02_22_Pro.jpg.jpg')}}" alt="">
                        {{-- {{url('uploads/news/5-lorem-ipsum-dolor-sit/thumbnail/WIN_20210803_02_02_22_Pro.jpg.jpg')}} --}}
                    </div>
                    <div class="entry-body">
                        <span class="cat">{{$post->created_at->isoFormat('dddd, D MMMM Y')}}</span>
                        <h3><a href="http://csshint.com/beautiful-css3-buttons-with-hover-effects/" target="_blank">{{$post->title}}</a></h3>
                        
                        <div class="read-more-date">
                            <a href="{{route('news.id', ['id'=>$post->id_post])}}" target="_blank">Read More..</a>
                            <span class="date">{{$post->updated_at->diffForHumans()}}</span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        
    </div>
    <div class="d-flex justify-content-center">
        {!! $posts->links() !!}
    </div>
    {{-- @foreach ($posts as $post)

    <article class="blog-post">
        <div class="featured-post">
            <a href="#">
                <img src="https://i.picsum.photos/id/1058/1200/600.jpg" alt="">
            </a>
        </div>
        <div class="divider25"></div>
        <div class="content">
            <h3 class="title">
                <a href="#">{{$post->title}}</a>
            </h3>
            <ul class="meta-post">
                <li class="date">
                    <a href="#">
                        {{$post->created_at->isoFormat('dddd, D MMMM Y')}}
                    </a>
                </li>
            </ul>
            <div class="entry-post">
                {{-- <p>This is a Rebel that surrendered to us. Although he denies it, I believe there may be more of them, and I request permission to conduct a further search of the area. He was armed only with this. Good work, Commander. Leave us. Conduct your search and bring his companions to me. Yes, my Lord.</p> --}}
                {{-- <div class="more-link">
                    <a href="#" class="read-more-btn">Read More</a>
                </div>
            </div> --}}
        {{-- </div> --}}
    {{-- </article>  
    @endforeach
    <div class="d-flex justify-content-center">
        {!! $posts->links() !!}
    </div> --}}
</div>
