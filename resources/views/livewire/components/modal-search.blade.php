<div>
    @if (!empty($contents))
        @php
            $title = ''
        @endphp
        @php
            $counter = 0
        @endphp
        @foreach ($contents as $ctn)
        @if(($ctn->title != $title) and ($counter == 0))
            @php
                $title = $ctn->title
            @endphp
            @php
                $counter++
            @endphp
        <div class="row">
            <div class="card-body">
                <div class="flex-fill">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h2 class="card-title">
                                        {{ $ctn->title }} - {{$ctn->dept_name}}
                                    </h2>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div id="accordion{{ $ctn->page }}" class="col-12">
                                            <div class="card card-primary card-outline">                                            
                                                <a class="d-block w-100" >
                                                    <div class="card-header">
                                                        <h4 class="card-title w-100">
                                                            {{ $ctn->title }} - Page {{ $ctn->page }}
                                                        </h4>
                                                    </div>
                                                </a>
                                                <div data-parent="#accordion{{ $ctn->page }}" style="">
                                                    <div class="card-body">
                                                        <livewire:components.landing.highlight-search :text="$ctn->content" :id_post="$ctn->id_post" :pageNum="$ctn->page" :searched="$search" :wire:key="$ctn->page">
                                                    </div>
                                                </div>                                            
                                            </div>
                                        </div>
                                    </div>
        @elseif($title == $ctn->title)
        <div class="row">
            <div id="accordion{{ $ctn->page }}" class="col-12">
                <div class="card card-primary card-outline">                                            
                    <a class="d-block w-100" >
                        <div class="card-header">
                            <h4 class="card-title w-100">
                                {{ $ctn->title }} - Page {{ $ctn->page }}
                            </h4>
                        </div>
                    </a>
                    <div data-parent="#accordion{{ $ctn->page }}" style="">
                        <div class="card-body" id="content-fetch">
                            <livewire:components.landing.highlight-search :text="$ctn->content" :id_post="$ctn->id_post" :pageNum="$ctn->page" :searched="$search" :wire:key="$ctn->page">
                        </div>
                    </div>                                            
                </div>
            </div>
        </div>
        @php
        $counter++
        @endphp
        @elseif(($ctn->title != $title) and ($counter != 0))
        @php
        $title = $ctn->title
        @endphp
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card-body">
                <div class="flex-fill">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h2 class="card-title">
                                        {{ $ctn->title }} Title : {{$title}}
                                    </h2>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div id="accordion{{ $ctn->page }}" class="col-12">
                                            <div class="card card-primary card-outline">                                            
                                                <a class="d-block w-100" >
                                                    <div class="card-header">
                                                        <h4 class="card-title w-100">
                                                            {{ $ctn->title }} - Page {{ $ctn->page }}
                                                        </h4>
                                                    </div>
                                                </a>
                                                <div data-parent="#accordion{{ $ctn->page }}" style="">
                                                    <div class="card-body">
                                                        <span>
                                                            
                                                        </span>
                                                        <livewire:components.landing.highlight-search :text="$ctn->content" :id_post="$ctn->id_post" :pageNum="$ctn->page" :searched="$search" :wire:key="$ctn->page">
                                                    </div>
                                                </div>                                            
                                            </div>
                                        </div>
                                    </div>
        @endif
        @endforeach
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
