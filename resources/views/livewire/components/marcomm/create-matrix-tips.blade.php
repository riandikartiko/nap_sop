<div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Preview</h4>  
                </div>
                <br>
                <div class="card-body">
                    @if ($preview == true)
                        {!! $description !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
            <div class="col-md-12">
                <form wire:submit.prevent="submit" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" wire:model.lazy="title" placeholder="Enter Title" required>
                        @error('title') <span class="text-danger">"Please insert a title!"</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="chooseFile">Image Thumbnail</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <label class="custom-file-label" for="chooseFile" x-data="{ files: null }">
                            <input type="file" class="sr-only" name="thumbnail" id="chooseFile" wire:model.lazy="thumbnail" x-on:change="files = Object.values($event.target.files)">
                            <span x-text="files ? files.map(file => file.name).join(', ') : 'Choose image..'"></span></label>
                            
                          </div>
                        </div>
                        @error('file') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group" wire:ignore>
                        <textarea type="text" name="description" input="description" id="summernote" class="form-control summernote" wire:model="description">
                            {{ $description }}
                        </textarea>
                    </div>
                    <div>
                        <div class="row">
                            @if (session()->has('success'))
                            <div class="col-8">
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            </div>
                            <script>
                                toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
                            </script>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <button class="btn btn-block btn-primary" type="submit">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-3">
                        <button class="btn btn-block btn-warning" type="submit" wire:click="draft">
                            Save as Draft
                        </button>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-block btn-info " type="submit" wire:click="getPreview">
                            Preview
                        </button>
                    </div>
                </div>
            </div>
    
    </div>
</div>
