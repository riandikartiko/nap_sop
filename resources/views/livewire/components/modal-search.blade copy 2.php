<div>
    @if(isset($contents) and (isset($search)))
        @php
            $tmpTitle = ''
        @endphp
        @php
            $counter = 0
        @endphp
        @foreach($contents as $ctn)
            @if(($ctn->title != $tmpTitle) and($counter == 0))
                @php
                    $tmpTitle = $ctn->title 
                @endphp
                @php
                    $counter++
                @endphp
                <div class="row">
                    <div id="accordion" class="col-12">
                        <div class="card card-primary card-outline">  
                            <a class="d-block w-100" >
                                <div class="card-header">
                                    <h4 class="card-title w-100">
                                        {{ $ctn->title }}
                                    </h4>
                                </div>
                            </a>
                            <div data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="card-body">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h2 class="card-title">Page {{ $ctn->page }}</h2>
                                                                <div class="card-tools">
                                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse" >
                                                                        <i class="fas fa-minus"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="card-body">
                                                                {{$ctn->content }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
            @elseif($ctn->title == $tmpTitle)
            <div class="row">
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h2 class="card-title">Page {{ $ctn->page }}</h2>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse" >
                                                <i class="fas fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        {{$ctn->content }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @elseif(($ctn->title != $tmpTitle) and($counter !== 0))
                @php
                    $tmpTitle = $ctn->title
                @endphp
                @php
                    $counter = 0
                @endphp
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="accordion" class="col-12">
                        <div class="card card-primary card-outline">  
                            <a class="d-block w-100" >
                                <div class="card-header">
                                    <h4 class="card-title w-100">
                                        {{ $ctn->title }}
                                    </h4>
                                </div>
                            </a>
                            <div data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="card-body">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h2 class="card-title">Page {{ $ctn->page }}</h2>
                                                                <div class="card-tools">
                                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse" >
                                                                        <i class="fas fa-minus"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="card-body">
                                                                {{$ctn->content }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
            @endif
        @endforeach
    @endif


</div>