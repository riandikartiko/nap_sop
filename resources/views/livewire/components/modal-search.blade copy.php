<div>
    @if(isset($contents))
    @foreach($contents as $ctn)
    <div class="row">
        <div id="accordion" class="col-12">
            <div class="card card-primary card-outline">                                            
                <a class="d-block w-100" >
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            {{ $ctn->title }} - Page {{ $ctn->page }}
                        </h4>
                    </div>
                </a>
                <div data-parent="#accordion">
                    <div class="card-body">
                        {{ $ctn->content }}
                    </div>
                </div>                                            
            </div>
        </div>                                              
    </div>
@endforeach 
@endif
</div>