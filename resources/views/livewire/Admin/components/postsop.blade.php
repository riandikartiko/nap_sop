
    <form wire:submit.prevent="submit">
        <div>
            <div class="row">
                @if (session()->has('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                <script>
                    toastr.success({{ session('success') }})
                </script>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" wire:model="title" placeholder="Enter Title">
            @error('title') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-8">
                    <label for="dept">Department</label>
                    <select class="form-control" name="dept" wire:model="dept" id="">
                            <option value="" hidden>Select a Department</option>
                        @foreach ($depts as $dept)                        
                            <option value="{{$dept->dept_name}}">{{$dept->dept_name}}</option>
                        @endforeach
                    </select>
                    @error('dept') <span class="text-danger">Please select a department!</span> @enderror
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="chooseFile">File</label>
            <div class="input-group">
              <div class="custom-file">
                <label class="custom-file-label" for="chooseFile" x-data="{ files: null }">
                <input type="file" class="sr-only" name="file" id="chooseFile" wire:model="file" x-on:change="files = Object.values($event.target.files)"  accept=".doc,.docx,.pdf">
                <span x-text="files ? files.map(file => file.name).join(', ') : 'Choose file..'"></span></label>

                <input type="file" name="file" id="chooseFile" wire:model="file">
              </div>
            </div>
            @error('file') <span class="text-danger">{{ $message }}</span> @enderror
        </div>
        @if ($file2)
            <livewire:components.sop.preview-pdf :previewFile="$file2" :wire:key="$file2->getClientOriginalName()">
        @endif
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
