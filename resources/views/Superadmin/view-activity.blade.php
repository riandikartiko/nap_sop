@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              {{-- <h1 class="m-0">Activity Log</h1> --}}
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-12">
              <div class="card card-cascade narrower z-depth-1">

                <!-- Card image -->
                <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
      
                  {{-- <div>
                    <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fas fa-th-large mt-0"></i></button>
                    <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fas fa-columns mt-0"></i></button>
                  </div> --}}
      
                  <a href="" class="white-text mx-3">Activity Logs</a>
{{--       
                  <div>
                    <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fas fa-pencil-alt mt-0"></i></button>
                    <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fas fa-eraser mt-0"></i></button>
                    <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2 waves-effect waves-light"><i class="fas fa-info-circle mt-0"></i></button>
                  </div> --}}
      
                </div>
                <!-- /Card image -->
      
                <div class="px-4">
      
                  <div class="table-responsive">
                    <!-- Table -->
                    <table class="table table-hover mb-0">
      
                      <!-- Table head -->
                      <thead>
                        <tr>
                          <th class="th-lg"><a>Name<i class="fas fa-sort ml-1"></i></a></th>
                          <th class="th-lg"><a href="">Activity<i class="fas fa-sort ml-1"></i></a></th>
                          <th class="th-lg"><a href="">Properties<i class="fas fa-sort ml-1"></i></a></th>
                          <th class="th-lg"><a href="">Time<i class="fas fa-sort ml-1"></i></a></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ( $activity_log as $act)                    
                        <tr>
                          <td>{{$act->user->name}}</td>
                          <td>{{$act->description}}</td>
                          <td>{{$act->properties}}</td>
                          <td>{{$act->created_at}}</td>
                        </tr>
                        @endforeach
                      </tbody>
                      <!-- Table body -->
                    </table>
                    <!-- Table -->
                  </div>
      
                  <hr class="my-0">
      
                  <!-- Bottom Table UI -->
                  <div class="d-flex justify-content-between">
      
                    <!-- Name -->
                    <select class="mdb-select colorful-select dropdown-primary mt-2">
                      <option value="" disabled="">Rows number</option>
                      <option value="1" selected="">5 rows</option>
                      <option value="2">25 rows</option>
                      <option value="3">50 rows</option>
                      <option value="4">100 rows</option>
                    </select>
      
                    <!-- Pagination -->
                    <nav class="my-4">
                      <ul class="pagination pagination-circle pg-blue mb-0">
      
                        <!-- First -->
                        <li class="page-item disabled clearfix d-none d-md-block"><a class="page-link waves-effect waves-effect">First</a></li>
      
                        <!-- Arrow left -->
                        <li class="page-item disabled">
                          <a class="page-link waves-effect waves-effect" aria-label="Previous">
                            <span aria-hidden="true">«</span>
                            <span class="sr-only">Previous</span>
                          </a>
                        </li>
      
                        <!-- Numbers -->
                        <li class="page-item active"><a class="page-link waves-effect waves-effect">1</a></li>
                        <li class="page-item"><a class="page-link waves-effect waves-effect">2</a></li>
                        <li class="page-item clearfix d-none d-md-inline-block"><a class="page-link waves-effect waves-effect">3</a></li>
                        <li class="page-item clearfix d-none d-md-inline-block"><a class="page-link waves-effect waves-effect">4</a></li>
                        <li class="page-item clearfix d-none d-md-inline-block"><a class="page-link waves-effect waves-effect">5</a></li>
      
                        <!-- Arrow right -->
                        <li class="page-item">
                          <a class="page-link waves-effect waves-effect" aria-label="Next">
                            <span aria-hidden="true">»</span>
                            <span class="sr-only">Next</span>
                          </a>
                        </li>
      
                        <!-- First -->
                        <li class="page-item clearfix d-none d-md-block"><a class="page-link waves-effect waves-effect">Last</a></li>
      
                      </ul>
                    </nav>
                    <!-- /Pagination -->
      
                  </div>
                  <!-- Bottom Table UI -->
      
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
</div>
@livewireScripts

<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
@endsection

