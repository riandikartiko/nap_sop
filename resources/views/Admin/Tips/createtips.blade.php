@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">Content Matrix Tips</h1>     
          </div>
        </div>
      </div>
    </div>
    <section class="content create">
      <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <livewire:components.marcomm.create-matrix-tips />
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
</div>
<link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/summernote-cleaner.js') }}"></script>
<script>
  $(function () {
    $('#summernote').summernote({
      tabsize: 3,
      height: 400,
      focus: true,
      dialogsInBody: true,
      toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline','italic', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture']],
          ['view', ['fullscreen', 'codeview', 'help']]
      ],
      styleTags: ['p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
      lineHeights: ['1.0', '1.2', '1.4', '1.5', '1.6', '1.8', '2.0', '3.0'],
      maximumImageFileSize: 2097152, 
      callbacks: {
        onChange: function(contents, $editable) {
          window.livewire.emit('refresh-contents', { content : contents});
        }
      },
      cleaner:{
          action: 'both', 
          newline: '<br>', 
          icon: '<i class="note-icon">[Your Button]</i>',
          keepHtml: false,
          keepOnlyTags: ['<p>', '<br>', '<ul>', '<li>', '<b>', '<strong>','<i>', '<a>'], 
          keepClasses: false,
          badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'],
          badAttributes: ['style', 'start'],
          limitChars: false, 
          limitDisplay: 'both',
          limitStop: false
      }
    })
  })
  function save() {
    var x = document.getElementById("summernote").value;
    console.log('onClick:', x);
    // window.livewire.emit('refresh-contents', { content : x});
  };
</script>
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@endsection

