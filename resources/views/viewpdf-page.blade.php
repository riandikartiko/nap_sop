@extends('Layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
          <div class="row mb-2">
          <div class="col-sm-12">
              <h1 class="m-0">{{$files->title}}</h1>
          </div>
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body" style="display: block;">
                <div>
                  <div class="row" style="overflow: auto;">
                    <div class="col-6">
                      <button id="prev" class="btn btn-block btn-primary" type="button">Previous</button>
                    </div>
                    <div class="col-6">
                      <button id="next" class="btn btn-block btn-primary" type="button">Next</button>
                    </div>
                  </div>
                  <div style="font-weight:bold;">
                    <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
                  </div>
                </div>
                <div>
                  <canvas id="the-canvas" style="border: 1px solid black; direction: ltr; width: 100%;"></canvas>
                </div>
              </div>
          <!-- /.card-body -->
              <div class="card-footer" style="display: block;">
                Footer
              </div>
          <!-- /.card-footer-->
            </div>
              <!-- /.card -->
          </div>
        </div>
          <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<script src="{{ asset('plugins/pdf.js/pdf.js') }}"></script>
<script id="worker" src="{{ asset('plugins/pdf.js/pdf.worker.js') }}"></script>
<script id="url" data-name="{{url($path->file_path)}}" page-number="{{$page}}" type="text/javascript" src="{{ url('/diy/js/viewpdf-page.js') }}"></script>
@endsection