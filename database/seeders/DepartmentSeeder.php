<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('depts')->insert([
            'dept_code' => 'rnc',
            'dept_name' => 'Risk and Compliance',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'mrc',
            'dept_name' => 'Marketing Communication',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'syd',
            'dept_name' => 'System Development',
        ]);//
        \DB::table('depts')->insert([
            'dept_code' => 'hcl',
            'dept_name' => 'Human Capital',
        ]);//
    }
}
