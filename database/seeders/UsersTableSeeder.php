<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'Riandi Kartiko',
            'email' => 'riandi.kartiko@napinfo.co.id',
            'is_active' => true,
            'password' => bcrypt('password'),
        ]);//
    }
}
