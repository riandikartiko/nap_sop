<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserDeptRelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('dept_user')->insert([
            'dept_id' => 1,
            'user_id' => 1,
        ]);//
    }
}
