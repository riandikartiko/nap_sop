<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePdfDescsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pdf_desc', function (Blueprint $table) {
            $table->id();
            $table->string('author')->nullable();
            $table->string('creator')->nullable();
            $table->string('pages');
            $table->bigInteger('id_pdf')->unsigned();
            $table->foreign('id_pdf')->references('id')->on('sop_pdf')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdf_desc');
    }
}
