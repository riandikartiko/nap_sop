<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePdfPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pdf_pages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_pdf')->unsigned();
            $table->foreign('id_pdf')->references('id')->on('sop_pdf')->onDelete('cascade');
            $table->string('page');
            $table->text('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdf_pages');
    }
}
