<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['app.locale'=>'id']);
        Carbon::setLocale('id');
        // setLocale(LC_TIME, $this->app->getLocale());

        setlocale(LC_TIME, 'id_ID');
        // \Carbon\Carbon::setLocale('id');
        Carbon::now()->formatLocalized("%A, %d %B %Y");

        //pagination
        Paginator::useBootstrap();
    }
}
