<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    use HasFactory;
    protected $table = 'announcements'; 
    protected $fillable = [
        'contents',
        'thumbnail_path',
        'status',
        'id_post',
        'is_update',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
