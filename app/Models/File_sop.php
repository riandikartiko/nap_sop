<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File_sop extends Model
{
    use HasFactory;
    protected $table = 'sop_pdf'; 
    protected $fillable = [
        'filename',
        'file_path',
        'status',
        'id_post',
        'is_update',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
