<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatrixTips extends Model
{
    // matrix_tips
    use HasFactory;
    protected $table = 'matrix_tips'; 
    protected $fillable = [
        'contents',
        'thumbnail_path',
        'status',
        'id_post',
        'is_update',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
