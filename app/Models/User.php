<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_active',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
      'created_at',
      'updated_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this
            ->belongsToMany(Role::class)
            ->withTimestamps();
    }
    public function users()
    {
        return $this
            ->belongsToMany(User::class)
            ->withTimestamps();
    }
    public function hasAnyRole($roles)
    {
      if (is_array($roles)) {
        foreach ($roles as $role) {
          if ($this->hasRole($role)) {
            return true;
          }
        }
      } else {
        if ($this->hasRole($roles)) {
          return true;
        }
      }
      return false;
    }

    public function hasRole($role)
    {
      if ($this->roles()->where('name', $role)->first()) {
        return true;
      }
      return false;
    }

    
    public function depts()
    {
        return $this
            ->belongsToMany(Dept::class)
            ->withTimestamps();
    }

    public function hasAnyDept($depts)
    {
      if (is_array($depts)) {
        foreach ($depts as $dept) {
          if ($this->hasDept($dept)) {
            return true;
          }
        }
      } else {
        if ($this->hasDept($depts)) {
          return true;
        }
      }
      return false;
    }

    public function hasDept($dept)
    {
      if ($this->depts()->where('dept_code', $dept)->first()) {
        return true;
      }
      return false;
    }

}
