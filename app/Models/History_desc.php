<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History_desc extends Model
{
    use HasFactory;
    protected $table = 'history_descs'; 
    protected $fillable = [
        'id_history',
        'desc',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
