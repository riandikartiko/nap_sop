<?php

namespace App\Http\Livewire\Superadmin;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\{
    User,Role, Dept,
    RelUserDept,
    RelUserRole, RelUserId,
};

class AddUser extends Component
{
    public $depts, $inputDept,
    $inputRole, $roles,
    $name, $email, $password, $deptPriv;


    protected $rules = [
        'name' => 'required|regex:/^[\pL\s\-]+$/u|min:6|max:80',
        'email' => 'required|email|unique:users',
        'inputDept' => 'required|exists:depts,dept_name',
        'inputRole' => 'required|exists:roles,name'

    ];
    public function mount()
    {
        $this->depts = Dept::get();
        $this->roles = Role::where('name', '<>','superadmin')->get();
        $this->deptPriv = Dept::whereIn('dept_name', array('Risk and Compliance', 'Marketing Communication'))->pluck('dept_name')->toArray();
        // $this->inputDept = $this->inputDept;
    }
    public function render()
    {
        if(!empty($this->inputDept))
        {
            $qRoles = DB::table('roles')->where('name', '<>','superadmin');
            if(!in_array($this->inputDept, $this->deptPriv))
            {
                $qRoles->where('name', '<>','admin');
            }
            $this->roles = $qRoles->get();
        }
        return view('livewire.superadmin.add-user');
    }

    public function submit()
    {
        $this->validate();
        $newUser = new User;
        $relUserRole = new RelUserRole;
        $relUserDept = new RelUserDept;

        DB::transaction(function () use (
            $newUser, $relUserRole, $relUserDept)
            {
                $findDept = Dept::where('dept_name',$this->inputDept)->firstOrFail('id');
                $findRole = Role::where('name', $this->inputRole)->firstOrFail('id');
                $newUser->name = ucwords($this->name);
                $newUser->email = $this->email;
                $newUser->is_active = true;
                $newUser->password = Hash::make($this->password);
                $newUser->save();

                $relUserRole->role_id = $findRole->id;
                $relUserRole->user_id = $newUser->id;
                $relUserDept->dept_id = $findDept->id;
                $relUserDept->user_id = $newUser->id;

                $relUserRole->save();
                $relUserDept->save();
                $this->resetInput();
                return redirect(request()->header('Referer'))->with('success', 'User Has Been Created - '.$newUser->email);
                // session()->flash('success', 'User Has Been Created - '.$newUser->email);
        });
    }
    private function resetInput()
    {

        $this->inputDept = null;
        $this->inputRole = null;
        $this->name = null;
        $this->email = null;
        $this->password = null;
        $this->preview = false;
    }
}
