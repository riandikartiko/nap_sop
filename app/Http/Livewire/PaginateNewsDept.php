<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PaginateNewsDept extends Component
{
    public function render()
    {
        return view('livewire.paginate-news-dept');
    }
}
