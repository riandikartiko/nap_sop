<?php

namespace App\Http\Livewire\Components\Additional;
use Livewire\Component;

class SuccessNotify extends Component
{
    public $messages;
    public function render()
    {
        return view('livewire.Components.Additional.success-notify');
    }	
}