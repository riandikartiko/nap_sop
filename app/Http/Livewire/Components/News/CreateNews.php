<?php

namespace App\Http\Livewire\Components\News;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\TextProcessingTrait;
use App\Models\{Posts,News,HistorySOP,
    History_desc, Dept,PostUserRel};
// use Purifier;

class CreateNews extends Component
{
    use WithFileUploads, TextProcessingTrait;

    public $title, $description, $thumbnail, $descriptionPreview, $thumbnailPreview;
    public $createdBy;
    public $preview = false;
    
    protected $listeners = [
        'refresh-contents' => 'getContent'
    ];
    private function resetInput()
    {
        $this->title = null;
        $this->description = null;
        $this->thumbnail = null;
        $this->descriptionPreview = null;
        $this->thumbnailPreview = null;
        $this->preview = false;
    }

    public function getContent($term)
    {
        $this->description= $term['content'];
        // clean($this->description)
    }
    public function render()
    {
        // dd(request()->header('Referer'));
        return view('livewire.create-news');
    }
    public function submit()
    {
        $postModel = new Posts;
        $newsModel = new News;
        $history = new HistorySOP;
        $history_desc = new History_desc;
        $user_post = new PostUserRel;
        DB::transaction(function () use (
            $postModel,$history,$history_desc,$newsModel,$user_post)
            {
                $postModel->title = $this->title;
                $postModel->id_dept = Dept::where('dept_code', 'mrc')->firstOrfail()->id;
                $postModel->seo_url = $this->string_seo($this->title);
                $postModel->save();
                
                $history->id_post = $postModel->id;
                $history->save();
                $history_desc->id_history = $history->id;
                $history_desc->desc = 'first upload';
                $history_desc->save();

                $fileName = $postModel->id.'-'.$this->seo_friendly_url($this->limit_text($this->title, 4));
                $dom = new \DomDocument();
                $dom->loadHtml($this->description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');
                foreach($images as $k => $img){
                    $data = $img->getAttribute('src');
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name = '/public/uploads/news/'.$fileName.'/contents'.'/'.
                    $fileName.'-'.time().$k.'.png';
                    // $path = url('/').str_replace('public','storage', $image_name);
                    $path = str_replace('public','storage', $image_name);
                    $path = preg_replace('/\/public/', '/storage', $image_name);
                    Storage::put($image_name, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $path);
                }
                $this->description = $dom->saveHTML();
                $newsModel->contents = $this->description;
                $newsModel->id_post = $postModel->id;
                $tl_path =  $this->thumbnail->storeAs('uploads/news/'.$fileName.'/'.'thumbnail', 
                    $this->thumbnail->getClientOriginalName().'.'.
                    $this->thumbnail->getClientOriginalExtension(), 'public');
                $newsModel->thumbnail_path = $tl_path;
                $newsModel->save();
                $user_post->id_post = $postModel->id;
                $user_post->id_user = auth()->user()->id;
                $user_post->name_user = auth()->user()->name;
                $user_post->save();
                $this->resetInput();
                return redirect()->route('news.title', ['title' => $postModel->seo_url])->with('success', 'Create News Content Success - '.$postModel->title);
                // return redirect(request()->header('Referer'))->with('success', 'News Has Been Created - '.$postModel->title);
                // session()->flash('success', 'Berhasil Membuat Berita Baru - '.$postModel->title);
        });
    }

    public function getPreview()
    {
        $this->preview = true;
        $this->descriptionPreview = $this->description;
        $this->thumbnailPreview = $this->thumbnail;
    }
}
