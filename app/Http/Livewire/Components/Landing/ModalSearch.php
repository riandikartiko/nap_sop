<?php

namespace App\Http\Livewire\Components\Landing;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Posts;

class ModalSearch extends Component
{
    use WithPagination;
        
    public $search;
    public $contents;
    public $limitPerPage = 6;
    protected $listeners = [
        'reset-search' => 'resetFilters',
        'search-result' => 'searchResults',
        'post-data' => 'postData',
        '$refresh',
    ];
    
    public function resetFilters()
    {
        $this->reset('search','contents');
        $this->reset();
    }
    public function searchResults($term)
    {
        $this->search= $term['search'];
    }
    public function postData()
    {
        $this->limitPerPage = $this->limitPerPage + 3;
    }

    public function queryResults($terms)
    {
        $contents = Posts::join('depts', 'posts.id_dept', '=', 'depts.id')
        ->join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
        ->join('pdf_pages as pp', 'sp.id', '=', 'pp.id_pdf')
        ->where('sp.status', 'active')
        ->where(function ($query) use ($terms) {
            foreach ($terms as $term) {
                $query->where('pp.content', 'LIKE', '%' . $term . '%')
                ->orWhere('posts.title','LIKE', '%'. $term. '%');
            }
        })
        ->orderBy('posts.title', 'asc')
        ->orderBy('sp.id', 'asc')
        ->orderBy('pp.id', 'asc')
        ->paginate($this->limitPerPage)->withQueryString();
        $posts = array();
        foreach($contents as $content)
        {
            array_push($posts, $content);
        }
        $this->contents = $posts;
    }
    public function render()
    {
        if(!empty($this->search)){
            $terms = array_filter(array_unique(explode(" ", $this->search)));
            $this->queryResults($terms);
        }else {
            $this->reset();
        }
        return view('livewire.components.modal-search');
    }
}
