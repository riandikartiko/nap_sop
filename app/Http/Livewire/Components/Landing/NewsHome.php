<?php

namespace App\Http\Livewire\Components\Landing;

use Livewire\Component;
use App\Models\{Posts};

class NewsHome extends Component
{
    public function render()
    {
        $query = Posts::join('news as nw', 'posts.id', '=', 'nw.id_post')
        // ->join('post_user_rels', 'posts.id', '=', 'post_user_rels.id_user')
        ->orderBy('nw.created_at', 'desc')
        ->take(4)
        ->get();
        return view('livewire.landing.news-home', [
            'news' => $query,
        ]);
    }
}
