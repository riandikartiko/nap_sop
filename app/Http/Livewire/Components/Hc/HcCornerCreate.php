<?php

namespace App\Http\Livewire\Components\Hc;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\TextProcessingTrait;
use App\Models\{Posts,HcCorner,PostUserRel, Dept};

class HcCornerCreate extends Component
{
    use WithFileUploads, TextProcessingTrait;

    public $title, $description, $thumbnail, $descriptionPreview, $thumbnailPreview;
    public $createdBy;
    public $preview = false;
    public function render()
    {
        return view('livewire.components.Hc.hc-corner-create');
    }
    protected $listeners = [
        'refresh-contents' => 'getContent'
    ];
    private function resetInput()
    {
        $this->title = null;
        $this->description = null;
        $this->thumbnail = null;
        $this->descriptionPreview = null;
        $this->thumbnailPreview = null;
        $this->preview = false;
    }

    public function getContent($term)
    {
        $this->description= $term['content'];
    }
    public function getPreview()
    {
        $this->preview = true;
        $this->descriptionPreview = $this->description;
        $this->thumbnailPreview = $this->thumbnail;
    }

    public function submit()
    {
        $postModel = new Posts;
        $contentModel = new HcCorner;
        $user_post = new PostUserRel;

        DB::transaction(function () use (
            $postModel,$contentModel,$user_post)
            {
                $postModel->title = $this->title;
                $postModel->id_dept = Dept::where('dept_code', 'hcl')->firstOrfail()->id;
                $postModel->seo_url = $this->string_seo($this->title);
                $postModel->save();
                $fileName = $postModel->id.'-'.$this->seo_friendly_url($this->limit_text($this->title, 4));
                $dom = new \DomDocument();
                $dom->loadHtml($this->description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');
                foreach($images as $k => $img){
                    $data = $img->getAttribute('src');
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name = '/public/uploads/hccorner/'.$fileName.'/contents'.'/'.
                    $fileName.'-'.time().$k.'.png';
                    // $path = url('/').str_replace('public','storage', $image_name);
                    $path = str_replace('public','storage', $image_name);
                    $path = preg_replace('/\/public/', '/storage', $image_name);
                    Storage::put($image_name, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $path);
                }
                $this->description = $dom->saveHTML();
                $contentModel->contents = $this->description;
                $contentModel->id_post = $postModel->id;
                $tl_path =  $this->thumbnail->storeAs('uploads/hccorner/'.$fileName.'/'.'thumbnail', 
                    $this->thumbnail->getClientOriginalName().'.'.
                    $this->thumbnail->getClientOriginalExtension(), 'public');
                $contentModel->thumbnail_path = $tl_path;
                $contentModel->save();
                $user_post->id_post = $postModel->id;
                $user_post->id_user = auth()->user()->id;
                $user_post->name_user = auth()->user()->name;
                $user_post->save();
                $this->resetInput();
                return redirect()->route('hc-corner.title', ['title' => $postModel->seo_url])->with('success', 'Content has been posted! '.$postModel->title);
                // return redirect(request()->header('Referer'))->with('success', 'Content Has Been Created - '.$postModel->title);
            });
    }
}
