<?php

namespace App\Http\Livewire\Components\Sop;

use Livewire\Component;
use Livewire\WithFileUploads;
use DomPDF;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\{
    Posts, File_sop, Dept, 
    HistorySOP, History_desc,PDFDesc,
    PDFPage,
};

use Illuminate\Support\Facades\DB;
class Updatesop extends Component
{

    use WithFileUploads;
    public $depts, $title, $dept, $file, $selected_id, $desc,$check;
    public $converted = null;

    protected $listeners = [
        'get-converted' => 'convertGet',
    ];
    protected $rules = [
        'title'=>'required',
        'dept'=>'required',
        'file' => 'required',
    ];

    public function mount()
    {
        $query = Posts::join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->join('depts', 'posts.id_dept', '=', 'depts.id')
        ->where('posts.id', $this->selected_id)
        ->firstOrFail();
        
        $this->title = $query->title;
        $this->dept = $query->dept_name;
    }
    public function render()
    {
        return view('livewire.Admin.components.updatesop');
    }

    public function convertGet($term)
    {
        $this->check = $term['check'];
        $this->converted = $term['filename'];
    }
    private function resetInput()
    {
        $this->file = null;
        $this->desc = null;
    }

    public function convert() {
        $tempname = preg_replace('/([?]expires).*/', '', basename($this->file->temporaryUrl()));
        $domPdfPath = base_path('vendor/dompdf/dompdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
        $convert = \PhpOffice\PhpWord\IOFactory::load(public_path('storage/uploads/livewire-tmp/'.$tempname)); 
        $PDFWriter = \PhpOffice\PhpWord\IOFactory::createWriter($convert,'PDF');
        $rmformat = preg_replace('/([.]docx|doc).*/','',$tempname).'.pdf';
        $PDFWriter->save(public_path('storage/uploads/livewire-tmp/'.$rmformat));
        // $this->converted = File::copy(public_path('storage/uploads/livewire-tmp/'.$rmformat),public_path('storage/uploads/'.$rmformat));
        $this->converted = $rmformat;
    }

    public function update()
    {
        $this->validate([
            'file' => 'required|mimes:pdf,docx,doc|max:4048',
            'title' => 'required|string',
            'dept' => 'required|exists:depts,dept_name',
            'desc' => 'required|string',
            'check' => 'required'
        ]);
        $fileModel = new File_sop;
        $history_desc = new History_desc;
        $pdf_descs = new PDFDesc;
        $parser = new \Smalot\PdfParser\Parser();
        $count_page = 1;
        DB::transaction(function () use (
            $fileModel,$history_desc,$pdf_descs,
            $parser,$count_page) 
            {
                if(in_array($this->file->getClientOriginalExtension(), array('docx','doc')))
                {
                    $fileModel->filename = time().'_'.
                    preg_replace('/\s+/', '_', $this->title).'.pdf';
                    File::copy(public_path('storage/uploads/livewire-tmp/'.$this->converted),public_path('storage/uploads/'.$fileModel->filename));
                    $fileModel->file_path = '/storage/uploads/'.$fileModel->filename;
                }else
                {
                    $fileModel->filename = time().'_'.
                        preg_replace('/\s+/', '_', $this->title).'.'.
                        $this->file->getClientOriginalExtension();
                    $filePath = $this->file->storeAs('uploads',$fileModel->filename, 'public');
                    $fileModel->file_path = '/storage/' . $filePath;

                }
                $fileModel->status = 'active';
                $fileModel->is_update = true;
                $fileModel->id_post = $this->selected_id;
                File_sop::where('filename', '!=', $fileModel->filename)
                    ->where('id_post', '=', $this->selected_id)
                    ->where('status', '=', 'active')
                    ->update(['status' => 'inactive']);
                $fileModel->save();
                $history = HistorySOP::where('id_post', '=', $this->selected_id)->firstOrFail();
                $history_desc->id_history = $history->id;
                $history_desc->desc = $this->desc;
                $history_desc->save();
                $parserPdf = $parser->parseFile(storage_path().'\app\public/'.trim($fileModel->file_path,"/storage"));
                $pdf_descs->id_pdf = $fileModel->id;
                $pdf_descs->author = isset($parserPdf->getDetails()['Author']) ? $parserPdf->getDetails()['Author'] : null;
                $pdf_descs->creator = isset($parserPdf->getDetails()['Creator']) ? $parserPdf->getDetails()['Creator'] : null;
                $pdf_descs->pages = $parserPdf->getDetails()['Pages'];
                $pdf_descs->save();

                $data_page = [];
                $forLoop = $parserPdf->getPages();
                foreach ($forLoop as $page) {
                    $pdf_page = new PDFPage;
                    $pdf_page->id_pdf = $fileModel->id;
                    $pdf_page->page = $count_page;
                    $pdf_page->content =  preg_replace('/\s+/', ' ',$page->getText());
                    $data_page[] = $pdf_page->attributesToArray();
                    $count_page++;
                }
                PDFPage::insert($data_page);
                // $pdfImg = new \Spatie\PdfToImage\Pdf(public_path('storage/uploads/'.$fileModel->filename));
                // $pdfImg->setPage(1)->saveImage(public_path('storage/uploads/img-convert'));
                $this->resetInput();
                session()->flash('success', 'Berhasil Update SOP');
        });
        // $this->resetInput();
    }

}
