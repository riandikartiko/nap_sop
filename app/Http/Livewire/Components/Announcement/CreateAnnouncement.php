<?php

namespace App\Http\Livewire\Components\Announcement;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\TextProcessingTrait;
use App\Models\{Posts,Announcement,HistorySOP,
    History_desc, Dept,PostUserRel};

class CreateAnnouncement extends Component
{
    use WithFileUploads, TextProcessingTrait;
    public $title, $description, $thumbnail, $descriptionPreview, $thumbnailPreview;
    public $createdBy;
    public $preview = false;
    protected $listeners = [
        'refresh-contents' => 'getContent'
    ];
    private function resetInput()
    {
        $this->title = null;
        $this->description = null;
        $this->thumbnail = null;
        $this->descriptionPreview = null;
        $this->thumbnailPreview = null;
        $this->preview = false;
    }

    public function getContent($term)
    {
        $this->description= $term['content'];
        // clean($this->description)
    }
    public function render()
    {
        // dd(request()->header('Referer'));
        return view('livewire.create-announcement');
    }
    public function submit()
    {
        $postModel = new Posts;
        $announcementModel = new Announcement;
        $history = new HistorySOP;
        $history_desc = new History_desc;
        $user_post = new PostUserRel;
        DB::transaction(function () use (
            $postModel,$history,$history_desc,$announcementModel,$user_post)
            {
                $postModel->title = $this->title;
                $postModel->id_dept = Dept::where('dept_code', 'mrc')->firstOrfail()->id;
                $postModel->seo_url = $this->string_seo($this->title);
                $postModel->save();
                $history->id_post = $postModel->id;
                $history->save();
                $history_desc->id_history = $history->id;
                $history_desc->desc = 'first upload';
                $history_desc->save();
                $fileName = $postModel->id.'-'.$this->seo_friendly_url($this->limit_text($this->title, 4));
                $dom = new \DomDocument();
                $dom->loadHtml($this->description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');
                foreach($images as $k => $img){
                    $data = $img->getAttribute('src');
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name = '/public/uploads/announcement/'.$fileName.'/contents'.'/'.
                    $fileName.'-'.time().$k.'.png';
                    // $path = url('/').str_replace('public','storage', $image_name);
                    $path = str_replace('public','storage', $image_name);
                    $path = preg_replace('/\/public/', '/storage', $image_name);
                    Storage::put($image_name, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $path);
                }
                $this->description = $dom->saveHTML();
                $announcementModel->contents = $this->description;
                $announcementModel->id_post = $postModel->id;
                $tl_path =  $this->thumbnail->storeAs('uploads/announcement/'.$fileName.'/'.'thumbnail', 
                    $this->thumbnail->getClientOriginalName().'.'.
                    $this->thumbnail->getClientOriginalExtension(), 'public');
                $announcementModel->thumbnail_path = $tl_path;
                $announcementModel->save();
                $user_post->id_post = $postModel->id;
                $user_post->id_user = auth()->user()->id;
                $user_post->name_user = auth()->user()->name;
                $user_post->save();
                $this->resetInput();
                // return redirect(request()->header('Referer'))->with('success', 'Announcement Has Been Created - '.$postModel->title);
                return redirect()->route('announcement.title', ['title' => $postModel->seo_url])->with('success', 'Announcement has been posted! - '.$postModel->title);

        });
    }
    public function getPreview()
    {
        $this->preview = true;
        $this->descriptionPreview = $this->description;
        $this->thumbnailPreview = $this->thumbnail;
    }
}
