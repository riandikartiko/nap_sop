<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Posts;
use Livewire\WithPagination;

class LiveTable extends Component
{
    use WithPagination;

    public $sortField = 'title'; // default sorting field
    public $sortAsc = true; // default sort direction
    public $search = '';

    public function render()
    {
        return view('livewire.live-table', [
            'posts' => Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
            ->join('depts', 'posts.id_dept', '=', 'depts.id')
            ->where('sp.status', 'active')
            ->where('title','LIKE', '%'.$this->search.'%')
            // ->get(['posts.id', 'posts.title','depts.dept_name', 'sp.status', 'sp.created_at','sp.id_post'])
            ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
            ->simplePaginate(10),
        ]);
        
        // return view('livewire.live-search');

    }
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }
}
