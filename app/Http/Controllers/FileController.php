<?php

namespace App\Http\Controllers;
use App\Models\{File_sop,Posts};
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function view($id)
    {
        $files = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->where('posts.id',$id)
        ->firstOrFail();
        $path = new Request(['file_path'=> url('view/isolate/'.trim($files->file_path,"/storage/uploads/"))]);
        return view('viewpdf',compact('files','path'));
    }
    
    public function viewIdPage($id, $page)
    {
        $files = Posts::join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->where('posts.id',$id)
        ->firstOrFail();
        // $page = $pageNum;
        $path = new Request(['file_path'=> url('view/isolate/'.trim($files->file_path,"/storage/uploads/"))]);
        return view('viewpdf-page',compact('files','path', 'page'));
    }
    public function viewSOPAll()
    {
        $sops = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        // ->join('post_user_rels', 'post_user_rels.id_post', '=', 'posts.id')
        ->orderBy('posts.created_at', 'desc')
        ->paginate(3);
        return view('Menu.Sop.newsdept', compact('sops'));
    }
    public function viewSOPDept($id)
    {
        $sops = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        // ->join('post_user_rels', 'post_user_rels.id_post', '=', 'posts.id')
        ->where('depts.id',$id)
        ->orderBy('posts.created_at', 'desc')
        ->paginate(2);
        return view('Menu.Sop.newsdept', compact('sops'));
    }
    public function isolate($string)
    {
        $getFiles = Storage::get('/public/uploads/'.$string);
        return $getFiles;
    }
    public function testView($string)
    {
        $files = new Request([
            'file_path'=> url('view/isolate/'.$string)
        ]);
        return view('viewpdf',compact('files'));
        // return view('home');
    }
}
