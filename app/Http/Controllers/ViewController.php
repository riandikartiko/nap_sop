<?php

namespace App\Http\Controllers;
use App\Models\{Posts};
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use App\Http\Traits\TextProcessingTrait;

class ViewController extends Controller
{
    use TextProcessingTrait;

    public function viewNews($title)
    {
        $news = Posts::join('depts', 'posts.id_dept', '=','depts.id')
            ->join('news', 'news.id_post', '=','posts.id')
            ->join('post_user_rels', 'posts.id', '=', 'post_user_rels.id_post')
            ->where('posts.seo_url', $title)
            ->firstOrFail();
        $by_user = strtolower($this->seo_friendly_url($news->name_user));
        return view('view-news',compact('news','by_user'));
    }

    public function newsByUser($string)
    {
        $query = Posts::join('post_user_rels', 'posts.id', 'post_user_rels.id_post')
        ->join('news', 'news.id', '=', 'post_user_rels.id_user')
        ->where('post_user_rels.name_user', 'like', '%'.$string.'%')
        ->paginate(10);
        $by = $string;
        // return $query;
        return view("Menu.News.news-user", compact('by'));
    }

    public function viewSOP($title)
    {
        $files = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->where('posts.seo_url',$title)
        ->firstOrFail();
        $path = new Request(['file_path'=> url('view/isolate/'.trim($files->file_path,"/storage/uploads/"))]);
        return view('viewpdf',compact('files','path'));
    }

    public function viewIdPage($id, $page)
    {
        $files = Posts::join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->where('posts.id',$id)
        ->firstOrFail();
        $path = new Request(['file_path'=> url('view/isolate/'.trim($files->file_path,"/storage/uploads/"))]);
        return view('viewpdf-page',compact('files','path', 'page'));
    }


    public function viewAnnouncement($title)
    {
        $announcement = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('announcements', 'announcements.id_post', '=','posts.id')
        ->where('posts.seo_url',$title)
        ->firstOrFail();
        return view('view-announcement',compact('announcement'));
    }
    public function viewHCorner($title)
    {
        $announcement = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('hc_corners', 'hc_corners.id_post', '=','posts.id')
        ->where('posts.seo_url',$title)
        ->firstOrFail();
        return view('view-announcement',compact('announcement'));
    }
    public function viewTips($title)
    {
        $announcement = Posts::join('depts', 'depts.id', '=','posts.id_dept')
        ->join('matrix_tips', 'matrix_tips.id_post', '=','posts.id')
        ->where('posts.seo_url',$title)
        ->firstOrFail();
        return view('view-announcement',compact('announcement'));
    }
}
