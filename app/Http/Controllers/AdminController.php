<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{
    Posts, File_sop, Dept, 
    HistorySOP, History_desc,PDFDesc,
    PDFPage,
};
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('role:admin');
    }
    public function upload()
    {
        $dept = Dept::get();
        return view("Admin.Upload.upload", compact('dept'));
    }
   public function update() 
   {
        $query = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
        ->join('history as h', 'posts.id', '=', 'h.id_post')
        ->join('depts as d', 'posts.id_dept', '=', 'd.id')
        ->join('history_sop_desc as hsd', 'h.id', 'hsd.id_history')
        ->get();

        return $query;
   }
   public function updateSOP(Request $req) 
   {
        $req->validate([
            'file' => 'required|mimes:pdf|max:4048',
            'title' => 'required|string',
            'dept' => 'required|exist:depts',
            'dept' => 'required',
        ]);
        $fileModel = new File_sop;
        $postModel = new Posts;
        $history = new HistorySOP;
        $history_desc = new History_desc;
        $pdf_descs = new PDFDesc;
        $parser = new \Smalot\PdfParser\Parser();
        $count_page = 1;
        try {
            DB::transaction(function () use (
                $postModel, $fileModel, $history, 
                $history_desc, $req,$pdf_descs,
                $parser,$count_page) { // Start the transaction
                $findDept = Dept::where('dept_name',$req->dept)
                ->firstOrFail('id');
                $postModel->title = $req->title;
                $postModel->id_dept = $findDept->id;
                $postModel->privacy = $req->privacy;
                $fileModel->filename = time().'_'.
                    preg_replace('/\s+/', '_', $req->title).'.'.
                    $req->file->getClientOriginalExtension();
                $fileModel->status = 'active';                    
                $postModel->save();
                $fileModel->id_post = $postModel->id;
                $filePath = $req->file('file')->storeAs('uploads',$fileModel->filename, 'public');
                $fileModel->file_path = '/storage/' . $filePath;
                $fileModel->save();
                $history->id_post = $postModel->id;
                $history->save();
                $history_desc->id_history = $history->id;
                $history_desc->desc = 'first upload';
                $history_desc->save();
                $parserPdf = $parser->parseFile(storage_path().'\app\public/'.trim($fileModel->file_path,"/storage"));
                $pdf_descs->id_pdf = $fileModel->id;
                $pdf_descs->author = $parserPdf->getDetails()['Author'];
                $pdf_descs->creator = $parserPdf->getDetails()['Creator'];
                $pdf_descs->pages = $parserPdf->getDetails()['Pages'];
                $pdf_descs->save();
                $data_page = [];
                $forLoop = $parserPdf->getPages();
                foreach ($forLoop as $page) {
                    $pdf_page = new PDFPage;
                    $pdf_page->id_pdf = $fileModel->id;
                    $pdf_page->page = $count_page;
                    $pdf_page->content =  preg_replace('/\s+/', ' ',$page->getText());
                    $data_page[] = $pdf_page->attributesToArray();
                    $count_page++;
                }
                PDFPage::insert($data_page);        
            }); 
            return back()
            ->with('success','File has been uploaded.')
            ->with('file');
        } catch (\Exception $e){
            return back()
            ->with('failed','File has not been uploaded.');
        }   


    }
}
