<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\Models\{User,Posts,File_sop,Dept, PDFDesc};
use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
    public function searchAction(Request $request){
        if($request->ajax()) 
        {
            $output = '';
            $query = $request->get('search');
            if($query != '')
            {
                $data = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
                ->join('depts', 'posts.id_dept', '=', 'depts.id')
                ->where('sp.status', 'active')
                ->get();
            }
            else {
                $data = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
                ->join('depts', 'posts.id_dept', '=', 'depts.id')
                ->where('sp.status', 'inactive')
                ->get();
            }
            $total_row = $data->count();
            if($total_row > 0)
            {
                foreach($data as $row)
                {
                $output .= '
                <tr>
                <td>'.$row->CustomerName.'</td>
                <td>'.$row->Address.'</td>
                <td>'.$row->City.'</td>
                <td>'.$row->PostalCode.'</td>
                <td>'.$row->Country.'</td>
                </tr>
                ';
                }
            }
            else
            {
             $output = '
             <tr>
              <td align="center" colspan="5">No Data Found</td>
             </tr>
             ';
            }
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row
            );
            echo json_encode($data);
        }
    }
}
