<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stevebauman\Location\Facades\Location;

class AuthController extends Controller
{
    public function login()
    {
        return view('Auth.login');
    }
    public function doLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        
        $check = User::where('email', $credentials['email'])->first();
        if(Auth::attempt($credentials)){
            if($check->is_active == true){
                $request->session()->regenerate();
                activity()->log('Login');   
                return redirect('/home');
            }
            else{
                return redirect('/')->with('statusLogin','Give Access First to User');
            }
        }
        else{
            return redirect('/')->with('statusLogin','Wrong Email or Password');
        }
    }
    
    public function doLogout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/home');
    }
}
