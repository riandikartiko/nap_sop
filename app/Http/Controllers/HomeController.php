<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\Models\{ActivityLog, User,Posts,File_sop,Dept, PDFDesc};
use Stevebauman\Location\Facades\Location;
use DataTables;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getPosts(){

        $data = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
        ->join('depts', 'posts.id_dept', '=', 'depts.id')
        ->where('sp.status', 'active');
        if(request('filter_dept')) {
            $data->where('depts.dept_name',request('filter_dept'));
        }
        if(request('filter_search')) {
            $data
            // ->where('depts.dept_name', 'like', '%' . request('filter_search') . '%')
            ->where('posts.title', 'like', '%' . request('filter_search') . '%');
        }
        $data = $data->get(['posts.id', 'posts.title','depts.dept_name', 'sp.status', 'sp.created_at','sp.id_post']);

        // return $data;
        $data2 = array();
        if(!empty($data))
        {
            foreach ($data as $d)
            {
                $show =  route('view.id',$d->id_post);
                $nestedData['title'] = $d->title;
                $nestedData['dept_name'] = $d->dept_name;
                $nestedData['status'] = strtoupper($d->status);
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($d->created_at));
                $nestedData['action'] = "
                <div class='row'>
                <div class='col-12'><a href='{$show}' title='SHOW' class=' btn btn-primary btn-sm' target='_blank'><i class='fas fa-book-open'></i>&emsp;View</a> </div>
                </div>
                ";
                $data2[] = $nestedData;
            }
        }
        return Datatables::of($data2)->make(true);
    }

    public function index(Request $request)
    {
        $dept =  Dept::get();
        // $announcement = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
        // ->where('sp.status', 'active')
        // ->orderBy('sp.created_at', 'desc')
        // ->take(3)
        // ->get();
        // return view('home-livewire', compact('dept', 'announcement'));
        // activity()->withProperties(['browser' => $request->server('HTTP_USER_AGENT'), 'ip' => $request->ip()])->log('Home Page');   
        return view('Menu.Landing.home-livewire', compact('dept'));
    }
    public function view()
    {
        $dept =  Dept::get();
        return view('sop-main', compact('dept'));
    }   
    public function dummy()
    {
        // $location = new Location();
        // $record = $location->get(request()->getClientIp());
        // dd($record);
        // $ip = $request->ip();
        
        // $currentUserInfo = Location::get($ip);  
        // return view('dummy', compact('currentUserInfo')); 
        
        // $userIp = $request->ip();
        // dd($userIp);
        // $locationData = \Location::get($userIp);
        
        // dd($locationData);

    }

}
