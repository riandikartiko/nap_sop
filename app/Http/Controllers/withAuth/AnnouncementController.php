<?php

namespace App\Http\Controllers\withAuth;
use App\Models\{Posts, Announcement};
use App\Http\Traits\TextProcessingTrait;

class AnnouncementController extends Controller
{
    use TextProcessingTrait;
    public function __construct()
    {
        $this->middleware(["role:admin", "dept:mrc"]);
    }

    public function create()
    {
        return view("Admin.Announcement.announcement");
    }


}
