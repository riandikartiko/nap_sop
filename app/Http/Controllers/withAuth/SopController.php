<?php

namespace App\Http\Controllers\withAuth;

use Illuminate\Http\Request;
use App\Models\{
    Posts, File_sop, Dept,
    HistorySOP, History_desc,
    PDFDesc, PDFPage,
};
use DataTables;
use Illuminate\Support\Facades\DB;


class SopController extends Controller
{
    public function __construct()
    {
        $this->middleware(["role:admin"]);
    }

    public function uploadView()
    {
        $dept = Dept::get();
        return view("Admin.Upload.upload", compact('dept'));
    }
   public function updateView() 
   {
        $remove = new \Illuminate\Filesystem\Filesystem;
        $remove->cleanDirectory(public_path('storage/uploads/livewire-tmp'));
        $dept = Dept::get();
        return view("Admin.Update.update", compact('dept'));
   }
   public function updateViewId($id) 
   {
        $posts = Posts::join('sop_pdf', 'sop_pdf.id_post', '=','posts.id')
        ->join('depts', 'posts.id_dept', '=', 'depts.id')
        ->where('posts.id',$id)
        ->firstOrFail();
        $path = new Request(['file_path'=> url('view/isolate/'.trim($posts->file_path,"/storage/uploads/"))]);
        return view("Admin.Update.updateViewId",compact('posts', $path));
   }
    public function getSOPDatatablesAdmin()
    {
        $data = Posts::join('sop_pdf as sp', 'posts.id', '=', 'sp.id_post')
        ->join('depts', 'posts.id_dept', '=', 'depts.id')
        ->where('sp.status', 'active');
        if(request('filter_dept')) {
            $data->where('depts.dept_name',request('filter_dept'));
        }
        if(request('filter_search')) {
            $data
            // ->where('depts.dept_name', 'like', '%' . request('filter_search') . '%') 
            ->where('posts.title', 'like', '%' . request('filter_search') . '%');
        }
        $data = $data->get(['posts.id', 'posts.title','depts.dept_name', 'sp.status', 'sp.created_at','sp.id_post']);

        $data2 = array();
        if(!empty($data))
        {
            foreach ($data as $d)
            {
                $show =  route('view.id',$d->id_post);
                $update =  route('updateViewId',$d->id_post);
                $nestedData['title'] = $d->title;
                $nestedData['dept_name'] = $d->dept_name;
                $nestedData['status'] = strtoupper($d->status);
                $nestedData['created_at'] = date('j M Y',strtotime($d->created_at));
                $nestedData['action'] = "
                <div class='btn-group'>
                <button type='button' class='btn btn-info'>Action</button>
                <button type='button' class='btn btn-info dropdown-toggle dropdown-hover dropdown-icon' data-toggle='dropdown' aria-expanded='false'>
                    <span class='sr-only'>Toggle Dropdown</span>
                </button>
                <div class='dropdown-menu' role='menu' style=''>
                    <a class='dropdown-item' href='{$show}' target='_blank'>View</a>
                    <a class='dropdown-item' href='{$update}' target='_blank'>Update</a>
                </div>
                </div>
                ";
                $data2[] = $nestedData;
            }
        }
        return Datatables::of($data2)->make(true);
    }

    public function fileUpload(Request $req)
    {
        $req->validate([
            'file' => 'required|mimes:pdf|max:4048',
            'title' => 'required|string',
            'dept' => 'required|exists:depts,dept_name',
            'privacy' => 'required|in:public,private'
        ]);
        $fileModel = new File_sop;
        $postModel = new Posts;
        $history = new HistorySOP;
        $history_desc = new History_desc;
        $pdf_descs = new PDFDesc;
        $parser = new \Smalot\PdfParser\Parser();
        $count_page = 1;
        try {
            DB::transaction(function () use (
                $postModel, $fileModel, $history, 
                $history_desc, $req,$pdf_descs,
                $parser,$count_page) { // Start the transaction
                $findDept = Dept::where('dept_name',$req->dept)
                ->firstOrFail('id');
                $postModel->title = $req->title;
                $postModel->id_dept = $findDept->id;
                $postModel->privacy = $req->privacy;
                $fileModel->filename = time().'_'.
                    preg_replace('/\s+/', '_', $req->title).'.'.
                    $req->file->getClientOriginalExtension();
                $fileModel->status = 'active';                    
                $postModel->save();
                $fileModel->id_post = $postModel->id;
                $filePath = $req->file('file')->storeAs('uploads',$fileModel->filename, 'public');
                $fileModel->file_path = '/storage/' . $filePath;
                $fileModel->save();
                $history->id_post = $postModel->id;
                $history->save();
                $history_desc->id_history = $history->id;
                $history_desc->desc = 'first upload';
                $history_desc->save();
                $parserPdf = $parser->parseFile(storage_path().'\app\public/'.trim($fileModel->file_path,"/storage"));
                $pdf_descs->id_pdf = $fileModel->id;
                $pdf_descs->author = $parserPdf->getDetails()['Author'];
                $pdf_descs->creator = $parserPdf->getDetails()['Creator'];
                $pdf_descs->pages = $parserPdf->getDetails()['Pages'];
                $pdf_descs->save();
                $data_page = [];
                $forLoop = $parserPdf->getPages();
                foreach ($forLoop as $page) {
                    $pdf_page = new PDFPage;
                    $pdf_page->id_pdf = $fileModel->id;
                    $pdf_page->page = $count_page;
                    $pdf_page->content =  preg_replace('/\s+/', ' ',$page->getText());
                    $data_page[] = $pdf_page->attributesToArray();
                    $count_page++;
                }
                PDFPage::insert($data_page);        
            }); 
            return back()
            ->with('success','File has been uploaded.')
            ->with('file');
        } catch (\Exception $e){
            return back()
            ->with('failed','File has not been uploaded.');
        }        
   }


}
