<?php

namespace App\Http\Controllers\withAuth;
use App\Models\{Posts, News};
use App\Http\Traits\TextProcessingTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    use TextProcessingTrait;
    public function __construct()
    {
        // $this->middleware(["role:admin", "dept:mrc"]);
    }
    public function view($title)
    {
        $news = Posts::join('depts', 'posts.id_dept', '=','depts.id')
            ->join('news', 'news.id_post', '=','posts.id')
            ->join('post_user_rels', 'posts.id', '=', 'post_user_rels.id_post')
            ->where('posts.seo_url', 'LIKE', '%'.$title.'%')
            ->firstOrFail();
        $by_user = strtolower($this->seo_friendly_url($news->name_user));
        return view('view-news',compact('news','by_user'));
    }
    public function createNews() 
    {
         return view("Admin.News.createnews");
    }

    public function allByUser($string)
    {
        // $query = Posts::join('post_user_rels', 'posts.id', 'post_user_rels.id_post')
        // ->join('news', 'news.id', '=', 'post_user_rels.id_user')
        // ->where('post_user_rels.name_user', 'like', '%'.$string.'%')
        // ->paginate(10);
        $by = $string;
        // return $query;
        return view("Menu.News.news-user", compact('by'));
    }
}
