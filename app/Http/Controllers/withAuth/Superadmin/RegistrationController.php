<?php

namespace App\Http\Controllers\withAuth\Superadmin;
use App\Models\{Posts, News};
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{    
    public function add() 
    {
         return view("Superadmin.add-user");
    }
}
