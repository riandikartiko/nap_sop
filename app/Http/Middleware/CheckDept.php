<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckDept
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$dept)
    {
        // dd($request->user()->hasRole('superadmin'));
        if (! ($request->user()->hasAnyDept($dept))) {
            if($request->user()->hasRole('superadmin'))
            {
                return $next($request);
            }
            abort(401, 'This action is unauthorized.');
        }

        return $next($request);
    }
}
