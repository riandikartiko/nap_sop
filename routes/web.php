<?php
use App\Http\Controllers\{
    HomeController,AdminController, FileController,
    AdminViewController, HistoryController,
    AuthController,ActivityLoggerController,
    ViewController,
};
use App\Http\Controllers\withAuth\{
    SopController,NewsController,AnnouncementController,
    HcController,
    MatrixTipsController,
};

use App\Http\Controllers\withAuth\Superadmin\{
    RegistrationController,ActivityLogController,
};
use Illuminate\Support\Facades\Route;
use Stevebauman\Location\Facades\Location;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/dologin', [AuthController::class, 'doLogin'])->name('doLogin');
Route::get('/logout', [AuthController::class, 'doLogin'])->name('logout');
Route::get('/home', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('/home/generate', [HomeController::class, 'getPosts'])->name('generate.home');
Route::get('/search', [HomeController::class, 'index']);

Route::prefix('view')->group(function () {
    // Route::get('sop', [HomeController::class, 'view']);
    Route::prefix('sop')->group(function () {
        Route::get('{title}', [ViewController::class, 'viewSOP'])->name('sop.title');
        Route::get('{id}/{page}', [ViewController::class, 'viewIdPage'])->name('view.id.page');
    });
    Route::prefix('news')->group(function () {
        Route::get('{title}', [ViewController::class, 'viewNews'])->name('news.title');
        Route::get('user/{string}', [ViewController::class, 'newsByUser'])->name('newsByUser');
        // Route::get('/sop/dept/{id}', [FileController::class, 'viewSOPDept'])->name('view.sop.dept');
    });
    Route::prefix('announcement')->group(function () {
        Route::get('{title}', [ViewController::class, 'viewAnnouncement'])->name('announcement.title');
    });
    Route::prefix('hc-corner')->group(function () {
        Route::get('{title}', [ViewController::class, 'viewHCorner'])->name('hc-corner.title');
    });
    Route::prefix('tips')->group(function () {
        Route::get('{title}', [ViewController::class, 'viewTips'])->name('tips.title');
        // Route::get('/sop/dept/{id}', [FileController::class, 'viewSOPDept'])->name('view.sop.dept');
    });

});


Route::get('/sop/dept/all', [FileController::class, 'viewSOPAll'])->name('view.allsop');
Route::get('/sop/dept/{id}', [FileController::class, 'viewSOPDept'])->name('view.sop.dept');
Route::get('/sop/history', [HistoryController::class, 'view'])->name('sop.history');
Route::get('/view/isolate/{filename}', [FileController::class, 'isolate']);


Route::group(['namespace' => 'withAuth'], function () {
    Route::prefix('admin')->group(function () {
        Route::middleware('dept:rnc')->group(function () {
            Route::get('sop/upload', [SopController::class, 'uploadView'])->name('viewUpload');
            Route::post('sop/upload', [SopController::class, 'fileUpload'])->name('uploadSOP');
            Route::get('sop/update', [SopController::class, 'updateView'])->name('viewUpdate');
            Route::get('sop/view/{id}', [SopController::class, 'updateViewId'])->name('updateViewId');
            Route::get('sop/fetch', [SopController::class, 'getSOPDatatablesAdmin'])->name('adminFetchSOP');
        });
        Route::middleware('dept:mrc')->group(function () {
            Route::get('news/create', [NewsController::class, 'createNews'])->name('createNews');
            Route::get('tips/create', [MatrixTipsController::class, 'createTips'])->name('createTips');
            Route::get('announcement/create', [AnnouncementController::class, 'create'])->name('crAnnouncement');
        });
        Route::middleware('dept:hcl')->group(function () {
            Route::get('corner/create', [HcController::class, 'createCorner'])->name('createCorner');
        });
        Route::get('news/create', [NewsController::class, 'createNews'])->name('createNews');
    });
});

Route::group(['namespace' => 'withAuth/Superadmin'], function () {
    Route::get('superadmin/add/user', [RegistrationController::class, 'add'])->name('add.user');
    Route::get('superadmin/log/activity', [ActivityLogController::class, 'view'])->name('view.activity');
    
});

Route::get('/dummy', [HomeController::class, 'dummy']);
Route::post('/activity-logger', [ActivityLoggerController::class, 'logging'])->name('logger');


// Route::post('/activity-logging', function () {
//     // dd(request()->getClientIp());
//     $currentUserInfo = request()->getClientIp();
//     return view('dummy', compact('currentUserInfo')); 
//     // dd(Location::get(request()->getClientIp()));
// });


Route::get('/ipaddress', function () {
    // dd(request()->getClientIp());
    $currentUserInfo = request()->getClientIp();
    return view('dummy', compact('currentUserInfo')); 
    // dd(Location::get(request()->getClientIp()));
});

